#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include <vector>
#include <boost/thread/thread.hpp>

using boost::asio::ip::tcp;
using namespace  std;
using namespace cv;

const int WIDTH = 480;
const int HEIGHT = 320;
const int CHANNELS = 1;
const int MAT_SIZE = WIDTH * HEIGHT * CHANNELS;

Mat img = Mat::zeros(WIDTH, HEIGHT, CV_8UC1);
bool flag = false;                              /* if flag is false ,the thread is not ready to show the mat frame */

void servershow()
{
    while (true)
    {
        if (flag)
        {
			if (img.cols == HEIGHT && img.rows == WIDTH) {
				imshow("server", img);
			}

            waitKey(20);
        }
    }
}

int main()
{
    boost::thread thrd(&servershow);

    try
    {
        boost::asio::io_service io_service;
        boost::array<char, MAT_SIZE> buf;
        tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 3200));

        while(1)
        {
            tcp::socket socket(io_service);
            acceptor.accept(socket);

            boost::system::error_code error;
			size_t len = boost::asio::read(socket, boost::asio::buffer(buf, MAT_SIZE), error);

            cout << "get data length :" << len << endl; /* disp the data size recieved */
            
			std::vector<uchar> vectordata(buf.begin(),buf.end());
            cv::Mat data_mat(vectordata,true);

            img = data_mat.reshape(CHANNELS, HEIGHT);

			Mat rotated;
			flip(img.t(), rotated, 0);

			img = rotated;

            cout << "reshape over" << endl;
            flag = true;
        }
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    thrd.join();
    return 0;
}
